// ! Bibliotecas
import 'package:flutter/material.dart';

// ! Função principal
void main() => runApp(const MyApp());

// ! Função "MyApp"
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // ! Retorna o widget "MaterialApp"
    return const MaterialApp(
      home: CalculadoraTitulo(),
    );
  }
}

class CalculadoraTitulo extends StatelessWidget {
  const CalculadoraTitulo({super.key});

  @override
  Widget build(BuildContext context) {
    // ! Widget que cria a tela principal do software
    return Scaffold(
      // ! Cor de fundo do container
      backgroundColor: Colors.black38,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text(
          'Calculadora',
          style: TextStyle(
            fontFamily: '',
            fontSize: 25.0,
            fontWeight: FontWeight.bold,
            color: Colors.white,
            letterSpacing: 1.0,
          ),
        ),
      ),
      body: const CalculadoraCorpo(),
    );
  }
}

class CalculadoraCorpo extends StatelessWidget {
  const CalculadoraCorpo({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: <Widget>[
        Card(
          color: Colors.white12,
          margin: EdgeInsets.symmetric(
            vertical: 3.0,
            horizontal: 4.0,
          ),
          child: SizedBox(
            height: 200.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      '50 + 90',
                      style: TextStyle(
                        fontFamily: 'Noto Serif',
                        fontSize: 60.0,
                        color: Colors.white,
                        letterSpacing: 1.0,
                        height: 4.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        // ! ...
        Row(
          children: <Widget>[
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    'AC',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '<=',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '%',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '/',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '7',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '8',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '9',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    'x',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '4',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '5',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '6',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '-',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '1',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '+',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 188.0,
                child: Center(
                  child: Text(
                    '0',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '.',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white24,
              margin: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 4.0,
              ),
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: Center(
                  child: Text(
                    '=',
                    style: TextStyle(
                      fontFamily: 'Noto Serif',
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
